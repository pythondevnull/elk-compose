#!/bin/bash

#from a node with metricbeat
metricbeat export template > metricbeat.template.json
mv metricbeat.template.json metricbeat-kafka.json

#from a node with connection to elasticsearch
curl -XPUT -u elastic -H 'Content-Type: application/json' https://developer2.dev.net:9200/_template/metricbeat-kafka -d@metricbeat-kafka.json --cacert /test-certs/ca/ca.crt

#verify the template exists
curl -XGET -u elastic "https://developer2.dev.net:9200/_template/metricbeat-kafka?pretty=true" --cacert /test-certs/ca/ca.crt
