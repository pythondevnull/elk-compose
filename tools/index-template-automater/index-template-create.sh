#!/bin/bash
curl -XPUT -u elastic --cacert /test-certs/ca/ca.crt "https://192.168.56.102:9200/_template/space_template_test1?pretty" -H 'Content-Type: application/json' -d'
{
    "index_patterns" : ["space-test*"],
    "version": 1,
    "settings" : {
        "number_of_shards" : 4,
        "number_of_replicas": 1,
        "index.lifecycle.name": "space_policy",
        "index.lifecycle.rollover_alias": "space-test"
    },
    "mappings": {
       "_source": {
          "enabled": true
       },
       "properties": {
          "planet_name": {
             "type": "keyword"
          },
          "discovered_at": {
             "type": "date",
             "format": "EEE MM dd HH:mm:ss Z yyyy"
          },
          "planet_size": {
             "type": "integer"
          },
          "planet_color": {
             "type": "keyword"
          },
          "planet_inhabitable": {
             "type": "boolean"
          }
       }
    }
}
'
